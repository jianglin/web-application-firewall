This folder contains all Flash we recorded for our WAF, you can check all our WAF function  by watching the Flash.

1.Exceeds Max Num of Parameter for Specific Page:
Screenshot for the case when number of parameters exceeds the max num of the this specific page. 
In our example, '/blog/public/user_main.php' has only one parameter at Training, but now it has two parameters(user_id and u).

2.Exceeds Max Num of Parameter for Whole Page:
Screenshot for the case when number of parameters exceeds the max num of all pages.
In this example, '/test.html' is never visited in Training.  And the max num of parameters accross all pages is 7.  
user access this page at Detection Mode with 8 parameters. 

3.Illegal CharacterSet:
Screenshot for the case when the parameter has illegal characters.
In this example, parameter 'username' in '/blog/public/user_signup.php' allows all letters and number, but this username contains character '@'.

4.Illegal Parameter Length:
Screenshot for the case when the parameter length exceeds its limit.
In this example, length of 'user_id' is 2, std=0.  This user_id length exceeds 2.

5.Unknown Parameter:
Screenshot for the case when the parameter is not seen in Train Mode.
In this example, '/blog/public/user_signup.php' has no parameter called "d" in Train Mode.

6.Signature GET:
Screenshot for the case when the GET request contains malicious string.
In this example, GET request contains "select" or "javascript", which is defined in WAF_Sig_Conf.

7.Signature POST:
Screenshot for the case when the POST request contains malicious string.
In this example, POST request contains "../../../../", which is defined in WAF_Sig_Conf.

8.Signature USER-AGENT:
Screenshot for the case when the HEADER request contains malicious string.
In this example, HEADER contains "bot", which is defined in WAF_Sig_Conf.

9.Train Start:
Screenshot to start training mode. And table info at that time.

10.Train Done:
Screenshot to stop training mode. And table info at that time.

